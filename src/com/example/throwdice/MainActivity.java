package com.example.throwdice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public class MainActivity extends Activity implements OnItemSelectedListener, OnClickListener {
	
	private Button btAt1, btAt2, btAt3, btAt4, btAt5, btDf1, btDf2, btDf3, btDf4, btDf5, btAtRoll, btDfRoll;
	private Spinner spAtSelect, spDfSelect, spRiskVersion;
	private ArrayAdapter<CharSequence> selectDiceNumAdapter, riskVersionAdapter;
	private int atDiceNum, dfDiceNum;
	private ArrayList<Integer> atDiceResult, dfDiceResult;
	private MediaPlayer mediaPlayer;
	private RelativeLayout mainLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
		
		btAt1 = (Button) findViewById(R.id.btAt1);
		btAt2 = (Button) findViewById(R.id.btAt2);
		btAt3 = (Button) findViewById(R.id.btAt3);
		btAt4 = (Button) findViewById(R.id.btAt4);
		btAt5 = (Button) findViewById(R.id.btAt5);
		
		btDf1 = (Button) findViewById(R.id.btDf1);
		btDf2 = (Button) findViewById(R.id.btDf2);
		btDf3 = (Button) findViewById(R.id.btDf3);
		btDf4 = (Button) findViewById(R.id.btDf4);
		btDf5 = (Button) findViewById(R.id.btDf5);
		
		btAtRoll = (Button) findViewById(R.id.btAtRoll);
		btDfRoll = (Button) findViewById(R.id.btDfRoll);
		btAtRoll.setOnClickListener(this);
		btDfRoll.setOnClickListener(this);
		
		spAtSelect = (Spinner) findViewById(R.id.spAtSelect);
		spDfSelect = (Spinner) findViewById(R.id.spDfSelect);
		spRiskVersion = (Spinner) findViewById(R.id.spRiskVersion);
		spAtSelect.setOnItemSelectedListener(this);
		spDfSelect.setOnItemSelectedListener(this);
		spRiskVersion.setOnItemSelectedListener(this);
		
		riskVersionAdapter = ArrayAdapter.createFromResource(this, R.array.risk_options, android.R.layout.simple_spinner_item);
		riskVersionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spRiskVersion.setAdapter(riskVersionAdapter);
		
		selectDiceNumAdapter = ArrayAdapter.createFromResource(this, R.array.throw_options, android.R.layout.simple_spinner_item);
		selectDiceNumAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spAtSelect.setAdapter(selectDiceNumAdapter);
		spDfSelect.setAdapter(selectDiceNumAdapter);
		
	}

	//SPINNERS LISTENERS
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int positon, long id) {
		if (parent == spAtSelect){
				switch (positon) {
				case 0:
					btAt1.setVisibility(View.VISIBLE);
					btAt2.setVisibility(View.INVISIBLE);
					btAt3.setVisibility(View.INVISIBLE);
					btAt4.setVisibility(View.INVISIBLE);
					btAt5.setVisibility(View.INVISIBLE);
					this.atDiceNum = 1;
					break;
				case 1:
					btAt1.setVisibility(View.VISIBLE);
					btAt2.setVisibility(View.VISIBLE);
					btAt3.setVisibility(View.INVISIBLE);
					btAt4.setVisibility(View.INVISIBLE);
					btAt5.setVisibility(View.INVISIBLE);
					this.atDiceNum = 2;
					break;
				case 2:
					btAt1.setVisibility(View.VISIBLE);
					btAt2.setVisibility(View.VISIBLE);
					btAt3.setVisibility(View.VISIBLE);
					btAt4.setVisibility(View.INVISIBLE);
					btAt5.setVisibility(View.INVISIBLE);
					this.atDiceNum = 3;
					break;
				case 3:
					btAt1.setVisibility(View.VISIBLE);
					btAt2.setVisibility(View.VISIBLE);
					btAt3.setVisibility(View.VISIBLE);
					btAt4.setVisibility(View.VISIBLE);
					btAt5.setVisibility(View.INVISIBLE);
					this.atDiceNum = 4;
					break;
				default:
					btAt1.setVisibility(View.VISIBLE);
					btAt2.setVisibility(View.VISIBLE);
					btAt3.setVisibility(View.VISIBLE);
					btAt4.setVisibility(View.VISIBLE);
					btAt5.setVisibility(View.VISIBLE);
					this.atDiceNum = 5;
					break;	
			}
		} else if (parent == spDfSelect){
				switch (positon) {
				case 0:
					btDf1.setVisibility(View.VISIBLE);
					btDf2.setVisibility(View.INVISIBLE);
					btDf3.setVisibility(View.INVISIBLE);
					btDf4.setVisibility(View.INVISIBLE);
					btDf5.setVisibility(View.INVISIBLE);
					this.dfDiceNum = 1;
					break;
				case 1:
					btDf1.setVisibility(View.VISIBLE);
					btDf2.setVisibility(View.VISIBLE);
					btDf3.setVisibility(View.INVISIBLE);
					btDf4.setVisibility(View.INVISIBLE);
					btDf5.setVisibility(View.INVISIBLE);
					this.dfDiceNum = 2;
					break;
				case 2:
					btDf1.setVisibility(View.VISIBLE);
					btDf2.setVisibility(View.VISIBLE);
					btDf3.setVisibility(View.VISIBLE);
					btDf4.setVisibility(View.INVISIBLE);
					btDf5.setVisibility(View.INVISIBLE);
					this.dfDiceNum = 3;
					break;
				case 3:
					btDf1.setVisibility(View.VISIBLE);
					btDf2.setVisibility(View.VISIBLE);
					btDf3.setVisibility(View.VISIBLE);
					btDf4.setVisibility(View.VISIBLE);
					btDf5.setVisibility(View.INVISIBLE);
					this.dfDiceNum = 4;
					break;
				default:
					btDf1.setVisibility(View.VISIBLE);
					btDf2.setVisibility(View.VISIBLE);
					btDf3.setVisibility(View.VISIBLE);
					btDf4.setVisibility(View.VISIBLE);
					btDf5.setVisibility(View.VISIBLE);
					this.dfDiceNum = 5;
					break;	
			}
		} else {
			switch (positon) {
			case 0: // Risk Normal seleccionado
				mainLayout.setBackgroundResource(R.drawable.risk_background);
				break;
			case 1: // Risk Tolkien seleccionado
				mainLayout.setBackgroundResource(R.drawable.tlotr_background);
				break;
			default:// Risk Star Wars seleccionado
				mainLayout.setBackgroundResource(R.drawable.swars_background);
				break;
			}
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	//BUTTONS LISTENERS
	@Override
	public void onClick(View v) {
		playDices();
		if (v == btAtRoll){
			atDiceResult=rollDice(atDiceNum);
			if(atDiceNum == 1){
				btAt1.setBackgroundResource(setRedDiceSide(atDiceResult.get(0)));
			}else if(atDiceNum == 2){
				btAt1.setBackgroundResource(setRedDiceSide(atDiceResult.get(0)));
				btAt2.setBackgroundResource(setRedDiceSide(atDiceResult.get(1)));
			}else if(atDiceNum == 3){
				btAt1.setBackgroundResource(setRedDiceSide(atDiceResult.get(0)));
				btAt2.setBackgroundResource(setRedDiceSide(atDiceResult.get(1)));
				btAt3.setBackgroundResource(setRedDiceSide(atDiceResult.get(2)));
			}else if(atDiceNum == 4){
				btAt1.setBackgroundResource(setRedDiceSide(atDiceResult.get(0)));
				btAt2.setBackgroundResource(setRedDiceSide(atDiceResult.get(1)));
				btAt3.setBackgroundResource(setRedDiceSide(atDiceResult.get(2)));
				btAt4.setBackgroundResource(setRedDiceSide(atDiceResult.get(3)));
			}else{
				btAt1.setBackgroundResource(setRedDiceSide(atDiceResult.get(0)));
				btAt2.setBackgroundResource(setRedDiceSide(atDiceResult.get(1)));
				btAt3.setBackgroundResource(setRedDiceSide(atDiceResult.get(2)));
				btAt4.setBackgroundResource(setRedDiceSide(atDiceResult.get(3)));
				btAt5.setBackgroundResource(setRedDiceSide(atDiceResult.get(4)));
			}
		}
		else{
			dfDiceResult=rollDice(dfDiceNum);
			if(dfDiceNum == 1){
				btDf1.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(0)));
			}else if(dfDiceNum == 2){
				btDf1.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(0)));
				btDf2.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(1)));
			}else if(dfDiceNum == 3){
				btDf1.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(0)));
				btDf2.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(1)));
				btDf3.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(2)));
			}else if(dfDiceNum == 4){
				btDf1.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(0)));
				btDf2.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(1)));
				btDf3.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(2)));
				btDf4.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(3)));
			}else{
				btDf1.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(0)));
				btDf2.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(1)));
				btDf3.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(2)));
				btDf4.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(3)));
				btDf5.setBackgroundResource(setBlackDiceSide(dfDiceResult.get(4)));
			}
		}
		
	}
	
	//Roll dices result
	private ArrayList<Integer> rollDice(int diceNum){
//		ArrayList<Integer> result = new ArrayList<Integer>();
//		for(int i=0;i<diceNum;i++){
//			int num = (int)Math.floor(Math.random()*(6-1+1)+1);
//			result.add(num);
//		}
//		Collections.sort(result);
//		System.out.println(result.toString());
		
		ArrayList<Integer> result = new ArrayList<Integer>();
		Random random = new Random();
		for(int i=0;i<diceNum;i++){
			int num = 1 + random.nextInt(6-1+1);
			result.add(num);
		}
		Collections.sort(result);
		System.out.println(result.toString());
		
		return result;
	}
	
	private int setRedDiceSide(int num){
		int result = 0;
		if(num == 1){
			result = R.drawable.red1;
		}else if(num == 2){
			result = R.drawable.red2;
		}else if(num == 3){
			result = R.drawable.red3;
		}else if(num == 4){
			result = R.drawable.red4;
		}else if(num == 5){
			result = R.drawable.red5;
		}else{
			result = R.drawable.red6;
		}
		return result;
	}
	
	private int setBlackDiceSide(int num){
		int result = 0;
		if(num == 1){
			result = R.drawable.black1;
		}else if(num == 2){
			result = R.drawable.black2;
		}else if(num == 3){
			result = R.drawable.black3;
		}else if(num == 4){
			result = R.drawable.black4;
		}else if(num == 5){
			result = R.drawable.black5;
		}else{
			result = R.drawable.black6;
		}
		return result;
	}
	
	private void playDices(){
		if(mediaPlayer == null){
			mediaPlayer = MediaPlayer.create(this, R.raw.dices_rolling);
		}
		if(!mediaPlayer.isPlaying()){
			mediaPlayer.start();
		}
	}
	
}







